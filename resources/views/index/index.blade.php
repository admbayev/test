<link rel="stylesheet" href="/css/style.css">
<script src="/js/jquery.min.js" type="text/javascript"></script>
<script src="/js/myjs.js" type="text/javascript"></script>

<div class="main-block">
    <meta name="csrf_token" content="{{ csrf_token() }}">
    <div class="small-block">

        <div class="fio-block">
            <p class="fio-block-title">Отправитель</p>
            <p class="fio-block-req">Выберите отправителя</p>
            @if(count($users_list) > 0)
                <select onchange="setSenderUserInfo(this)" class="fio-select">
                    <option value="0"></option>
                    @foreach($users_list as $key => $user_item)
                        <option value="{{$user_item->id}}">
                            {{$user_item->name}}
                        </option>
                    @endforeach
                </select>
            @endif
            <p class="balans-p">Баланс: <span class="balans-span">0</span> тенге</p>
        </div>

        <div class="translate-block">
            <p class="sum-p">Сумма перевода</p>
            <form id="myform" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" value="0" id="sender_user_id" name="sender_user_id">
                <input type="hidden" value="0" id="receiver_user_id" name="receiver_user_id">
                <input type="text" name="send_balans_sum" id="send_balans_sum" class="send-balans-input"> <span class="kzt">KZT</span> <br>
                <input type="button" value="Отправить" class="send-btn" onclick="makeTransaction()">
            </form>
        </div>

        <div class="fio-block">
            <p class="fio-block-title">Получатель</p>
            <p class="fio-block-req">Выберите получателя</p>
            @if(count($users_list) > 0)
                <select onchange="setReceiverUserInfo(this)" class="fio-select">
                    <option value="0"></option>
                    @foreach($users_list as $key => $user_item)
                        <option value="{{$user_item->id}}">
                            {{$user_item->name}}
                        </option>
                    @endforeach
                </select>
            @endif
            <p class="balans-p">Баланс: <span class="receiver-balans-span">0</span> тенге</p>
        </div>

        <div class="clearfloat"></div>
    </div>
</div>