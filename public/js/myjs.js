function setSenderUserInfo(ob){
    id = $(ob).val();

    $.ajax({
        type: 'post',
        url: "/get-user-info",
        data: {_token: $('[name="csrf_token"]').attr('content'), id: id},
        success: function(data){
            if(data.result == false){
                alert("Ошибка при получении данных");
            }
            else{
                $("#sender_user_id").attr("value",id);
                $(".balans-span").html(data.result['balance']);
            }
        }
    });
}

function setReceiverUserInfo(ob){
    id = $(ob).val();

    $.ajax({
        type: 'post',
        url: "/get-user-info",
        data: {_token: $('[name="csrf_token"]').attr('content'), id: id},
        success: function(data){
            if(data.result == false){
                alert("Ошибка при получении данных");
            }
            else{
                $("#receiver_user_id").attr("value",id);
                $(".receiver-balans-span").html(data.result['balance']);
            }
        }
    });
}

function makeTransaction(){
    $.ajax({
        type: 'post',
        url: "/make-transaction",
        data: {_token: $('[name="csrf_token"]').attr('content'), send_balans_sum: $("#send_balans_sum").val(), sender_user_id: $("#sender_user_id").val(), receiver_user_id: $("#receiver_user_id").val()},
        success: function(data){
            if(data.result == "NOT_CORRECT_SUM"){
                alert("Введите сумму");
            }
            else if(data.result == "NOT_SELECT_SENDER"){
                alert("Выберите отправителя");
            }
            else if(data.result == "NOT_SELECT_RECEIVER"){
                alert("Выберите получателя");
            }
            else{
                $(".balans-span").html(parseFloat($(".balans-span").html()) - parseFloat($("#send_balans_sum").val()));
                $(".receiver-balans-span").html(parseFloat($(".receiver-balans-span").html()) + parseFloat($("#send_balans_sum").val()));
                $("#send_balans_sum").val(0);
                alert("Транзакция успешно произведена");
            }
        }
    });
}