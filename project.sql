﻿/* SQL Manager Lite for MySQL                              5.6.0.47256 */
/* ------------------------------------------------------------------- */
/* Host     : localhost                                                */
/* Port     : 3306                                                     */
/* Database : project                                                  */


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES 'utf8' */;

SET FOREIGN_KEY_CHECKS=0;

CREATE DATABASE `project`
    CHARACTER SET 'utf8'
    COLLATE 'utf8_general_ci';

/* Структура для таблицы `comments`:  */

CREATE TABLE `comments` (
  `id` INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INTEGER(10) UNSIGNED NOT NULL,
  `text` TEXT COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY USING BTREE (`id`)
) ENGINE=InnoDB
AUTO_INCREMENT=9 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `users`:  */

CREATE TABLE `users` (
  `id` INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) COLLATE utf8_general_ci NOT NULL,
  `balance` DECIMAL(10,2) NOT NULL,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY USING BTREE (`id`)
) ENGINE=InnoDB
AUTO_INCREMENT=6 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Data for the `comments` table  (LIMIT 0,500) */

INSERT INTO `comments` (`id`, `user_id`, `text`) VALUES
  (1,1,'Hi\r\n'),
  (2,1,'Hello'),
  (3,1,'Whatsup'),
  (4,2,'Kill'),
  (5,3,'Whoa'),
  (6,3,'He-he'),
  (7,4,'Zidan'),
  (8,5,'Arteta');
COMMIT;

/* Data for the `users` table  (LIMIT 0,500) */

INSERT INTO `users` (`id`, `name`, `balance`, `created_at`, `updated_at`) VALUES
  (1,'Adik',19000.00,'2016-08-04 02:40:27','2016-08-03 20:54:04'),
  (2,'Olzhas',13000.00,'2016-08-04 02:40:27','2016-08-03 21:06:59'),
  (3,'Dias',26000.00,'2016-08-04 02:40:27','2016-08-03 21:06:59'),
  (4,'Tatyana',100000.00,'2016-08-04 02:40:27','0000-00-00 00:00:00'),
  (5,'Abyl',4000.00,'2016-08-04 02:40:27','2016-08-03 20:44:07');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;