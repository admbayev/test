<?php

namespace App\Http\Controllers\Index;
use App\Http\Controllers\Controller;
use App\Models\Users;
use Illuminate\Http\Request;
use App\Http\Requests;

class IndexController extends Controller
{
    public function mainPage(){
        $users_list = Users::all();
        return view('index.index', ['users_list' => $users_list]);
    }

    public function getUserInfo(Request $request){
        $user_row = Users::where("id","=",$request->id)->first();
        return response()->json(['result'=>$user_row]);
    }

    public function makeTransaction(Request $request){
        if($request->send_balans_sum < 1){
            return response()->json(['result'=>"NOT_CORRECT_SUM"]);
        }
        else if($request->sender_user_id < 1){
            return response()->json(['result'=>"NOT_SELECT_SENDER"]);
        }
        else if($request->receiver_user_id < 1){
            return response()->json(['result'=>"NOT_SELECT_RECEIVER"]);
        }
        else{
            $sender_row = Users::where("id","=",$request->sender_user_id)->first();
            $sender_row->balance -= $request->send_balans_sum;
            $sender_row->save();

            $receiver_row = Users::where("id","=",$request->receiver_user_id)->first();
            $receiver_row->balance += $request->send_balans_sum;
            $receiver_row->save();
            return response()->json(['result'=>true]);
        }

    }
}
