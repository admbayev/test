<?php

Route::group(['namespace' => 'Index'], function()
{
    Route::get('/', 'IndexController@mainPage');
    Route::post('/get-user-info', 'IndexController@getUserInfo');
    Route::post('/make-transaction', 'IndexController@makeTransaction');
});